import {makeAutoObservable} from "mobx";

export default class DeviceStore{
    constructor() {
        this._types = [
            {id: 1, name: 'Холодильники'},
            {id: 2, name: 'Смартфоны'},
            {id: 3, name: 'Ноутбуки'},
            {id: 4, name: 'Телевизоры'}
        ];
        this._brands = [
            {id: 1, name: 'Samsung'},
            {id: 2, name: 'Apple'},
            {id: 3, name: 'Lenovo'},
            {id: 4, name: 'Xiaomi'},
            {id: 5, name: 'Bosh'},
            {id: 6, name: 'Philips'},
        ]
        this._devices = [
            {id: 1, name: "Iphone 12 pro",rating: 5, price: 25000, img: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcShbbKwWijZ-4h-0Awun0PMekzARXvaORvNhQ&usqp=CAU'},
            {id: 2, name: "Iphone 12 pro",rating: 4, price: 25000, img: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcShbbKwWijZ-4h-0Awun0PMekzARXvaORvNhQ&usqp=CAU'},
            {id: 3, name: "Iphone 12 pro",rating: 2, price: 25000, img: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcShbbKwWijZ-4h-0Awun0PMekzARXvaORvNhQ&usqp=CAU'},
            {id: 4, name: "Iphone 12 pro",rating: 5, price: 25000, img: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcShbbKwWijZ-4h-0Awun0PMekzARXvaORvNhQ&usqp=CAU'},
            {id: 5, name: "Iphone 12 pro",rating: 1, price: 25000, img: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcShbbKwWijZ-4h-0Awun0PMekzARXvaORvNhQ&usqp=CAU'},
            {id: 6, name: "Iphone 12 pro",rating: 1, price: 25000, img: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcShbbKwWijZ-4h-0Awun0PMekzARXvaORvNhQ&usqp=CAU'},
            {id: 7, name: "Iphone 12 pro",rating: 1, price: 25000, img: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcShbbKwWijZ-4h-0Awun0PMekzARXvaORvNhQ&usqp=CAU'},
            {id: 8, name: "Iphone 12 pro",rating: 1, price: 25000, img: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcShbbKwWijZ-4h-0Awun0PMekzARXvaORvNhQ&usqp=CAU'},
            {id: 9, name: "Iphone 12 pro",rating: 1, price: 25000, img: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcShbbKwWijZ-4h-0Awun0PMekzARXvaORvNhQ&usqp=CAU'},
            {id: 10, name: "Iphone 12 pro",rating: 1, price: 25000, img: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcShbbKwWijZ-4h-0Awun0PMekzARXvaORvNhQ&usqp=CAU'},
        ]
        this._selectedType = {}
        this._selectedBrand = {}
        makeAutoObservable(this);
    }

    setTypes(types) {
        this._types = types;
    }
    setBrands(brands) {
        this._brands = brands;
    }
    setDevices(devices) {
        this._devices = devices;
    }

    setSelectedType(type){
        this._selectedType = type;
    }
    setSelectedBrand(brand){
        this._selectedBrand = brand;
    }

    get types(){
        return this._types;
    }

    get brands(){
        return this._brands;
    }

    get devices(){
        return this._devices;
    }

    get selectedType(){
        return this._selectedType;
    }
    get selectedBrand(){
        return this._selectedBrand;
    }
}
