import React from "react";
import { Col, Container, Image, Row } from "react-bootstrap";
import "../styles/devicePage.scss";
import Stars from "../UI/Stars";

const DevicePage = () => {
  const device = {
    id: 1, 
    name: "Iphone 12 pro",
    rating: 3,
    price: 25000,
    img: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcShbbKwWijZ-4h-0Awun0PMekzARXvaORvNhQ&usqp=CAU",
  };
  const deviceDescriptions = [
    {description: 'Емкость', value: "64 Гб"},
    {description: 'Вес', value: "194 г"},
    {description: 'Дисплей', value: '6.1"'},
    {description: 'Процессор', value: 'A13'},
    {description: 'Камера', value: '12 МП + 12 МП'},
    {description: 'Съемка видео', value: '4К'},
  ]
  return (
    <Container className="mt-3">
      <div className="device-page">
        <Row>
          <Col md={6} className="device-page__block">
            <Image src={device.img} height={400} width={300} />
          </Col>
          <Col md={6} className="device-page__content">
            <h2 className="device-page__name">{device.name}</h2>
            <div className="device-page__info info">
              <div className="info__price">{device.price} руб.</div>
              <Stars count={device.rating} />
            </div>
            <div className="device-page__text">
              Lorem ipsum dolor sit amet consectetur adipisicing elit. Facere,
              dignissimos! Accusamus molestias architecto aliquam quod nostrum
              non a dolor laborum omnis asperiores, reiciendis eveniet ex eos
              vel cupiditate voluptate quo?
            </div>
            <button className="device-page__buy">
              <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512">
                <path d="M24 0C10.7 0 0 10.7 0 24S10.7 48 24 48H76.1l60.3 316.5c2.2 11.3 12.1 19.5 23.6 19.5H488c13.3 0 24-10.7 24-24s-10.7-24-24-24H179.9l-9.1-48h317c14.3 0 26.9-9.5 30.8-23.3l54-192C578.3 52.3 563 32 541.8 32H122l-2.4-12.5C117.4 8.2 107.5 0 96 0H24zM176 512c26.5 0 48-21.5 48-48s-21.5-48-48-48s-48 21.5-48 48s21.5 48 48 48zm336-48c0-26.5-21.5-48-48-48s-48 21.5-48 48s21.5 48 48 48s48-21.5 48-48z" />
              </svg>
              Add to cart
            </button>
          </Col>
        </Row>
        <Row style={{margin: 0}}>
                {deviceDescriptions.map((info, idx) => 
                    <Row key={idx} style={{background: `${idx % 2 === 0 ? 'lightgray': 'transparent'}`, padding: 10}}>
                        {info.description}: {info.value}
                    </Row>
                )}
        </Row>
      </div>
    </Container>
  );
};

export default DevicePage;
