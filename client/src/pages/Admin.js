import React, { useState } from "react";
import { Button, Container } from "react-bootstrap";
import CreateBrand from "../components/modals/CreateBrand";
import CreateDevice from "../components/modals/CreateDevice";
import CreateType from "../components/modals/CreateType";

const Admin = () => {
  const [typesVisible, setTypesVisible] = useState(false);
  const [brandVisible, setBrandVisible] = useState(false);
  const [deviceVisible, setDeviceVisible] = useState(false);

  return (
    <Container className="d-flex flex-column">
      <Button variant={"outline-dark"} className="mt-4" onClick={() => setTypesVisible(true)}>
        Добавить тип
      </Button>
      <Button variant={"outline-dark"} className="mt-4" onClick={() => setBrandVisible(true)}>
        Добавить бренд
      </Button>
      <Button variant={"outline-dark"} className="mt-4" onClick={() => setDeviceVisible(true)}>
        Добавить утройство
      </Button>
      <CreateType show={typesVisible} onHide={() => setTypesVisible(false)}/>
      <CreateBrand show={brandVisible} onHide={() => setBrandVisible(false)}/>
      <CreateDevice show={deviceVisible} onHide={() => setDeviceVisible(false)}/>

    </Container>
  );
};

export default Admin;
