import React from 'react';
import {Col, Row} from "react-bootstrap";
import TypeBar from "../components/TypeBar";
import BrandBar from "../components/brandBar";
import DeviceList from "../components/DeviceList";

const Shop = () => {
    return (
         <Row className="mt-4">
             <Col md={3}>
                 <TypeBar/>
             </Col>
             <Col md={9}>
                 <BrandBar/>
                 <DeviceList/>
             </Col>
         </Row>
    );
};

export default Shop;
