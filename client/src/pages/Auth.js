import React from 'react';
import '../styles/authForm.scss'
import {NavLink, useLocation} from "react-router-dom";
import {LOGIN_ROUTE, REGISTRATION_ROUTE} from "../utils/consts";

const Auth = () => {
    const location = useLocation();
    const isLogin = location.pathname === LOGIN_ROUTE;

    return (
        <div className="form-container">
            <form className="form">
                <h1 className="form__title">{isLogin? "Авторизация" : "Регистрация"}</h1>
                <div className="form__group">
                    <input className="form__input" placeholder=" " name="login"/>
                    <label className="form__label">Email</label>
                </div>
                <div className="form__group">
                    <input type="password" className="form__input" placeholder=" " name="password"/>
                    <label className="form__label">Password</label>
                </div>
                <div className="form__auth">
                    {isLogin?
                        <div className="form__create-account">Нет аккаунта?<NavLink to={REGISTRATION_ROUTE}>Зарегистрируйся!</NavLink></div>
                        : <div className="form__create-account">Есть аккаунт?<NavLink to={LOGIN_ROUTE}>Войдите!</NavLink></div>
                    }
                    <button className="form__button">{isLogin? "Войти" : "Регистрация"}</button>
                </div>


            </form>
        </div>
    );
};

export default Auth;
