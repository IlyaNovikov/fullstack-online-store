import React, {useContext} from 'react';
import {observer} from "mobx-react-lite";
import {Context} from "../index";
import ListGroup from "react-bootstrap/ListGroup";
import '../styles/devices.scss'

const TypeBar = () => {
    const {device} = useContext(Context)
        return (
        <ListGroup>
            {device.types.map(type =>
                <ListGroup.Item
                    className="devices__types"
                    active={type.id === device.selectedType.id}
                    onClick={() => device.setSelectedType(type)}
                    key={type.id}
                >
                    {type.name}
                </ListGroup.Item>
            )}
        </ListGroup>
    );
};

export default observer(TypeBar);
