import React from 'react';
import {Card, Col, Image} from "react-bootstrap";
import '../styles/device.scss'
import {useNavigate} from "react-router-dom";
import {DEVICE_ROUTE} from "../utils/consts";
import Stars from "../UI/Stars";

const DeviceItem = ({device}) => {
    const navigate = useNavigate()

    return (
        <Col md={3} className="device" border={"light"} onClick={() => navigate(DEVICE_ROUTE + '/' + device.id)}>
            <Card style={{cursor: "pointer", marginTop: '2em', border: 'none'}}>
                <div className="device__block">
                    <div className="device__image-block">
                        <Image className="device__image" width={150} height={150} src={device.img}/>
                    </div>
                    <div className="device__info">
                        <div className="device__content">
                            <div>Iphone</div>
                            <div className="device__rating">
                                <Stars count={device.rating}/>
                            </div>
                        </div>
                        <div className="device__name">{device.name}</div>
                    </div>

                </div>

            </Card>
        </Col>
    );
};

export default DeviceItem;
