import React, {useContext} from 'react';
import {observer} from "mobx-react-lite";
import {Context} from "../index";
import {Card, Row} from "react-bootstrap";

const BrandBar = observer(() => {
    const {device} = useContext(Context)
    return (
        <Row style={{display: "flex"}}>
            {device.brands.map(brand =>
                <Card
                    style={{cursor: "pointer", width: "auto", margin: "0 8px"}}
                    border={brand.id === device.selectedBrand.id ? "danger": "light"}
                    className="p-3"
                    onClick={() => device.setSelectedBrand(brand)}
                    key={brand.id}
                >
                    {brand.name}
                </Card>
            )}
        </Row>
    );
});

export default BrandBar;
