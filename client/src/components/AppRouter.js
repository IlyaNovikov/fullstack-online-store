import React, { useContext } from "react";
import { Route, Routes, Navigate } from "react-router-dom";
import { authRoutes, publicRoutes } from "../routes";
import { Context } from "../index";

const AppRouter = () => {
  const { user } = useContext(Context);

  return (
    <div>
      {user.isAuth ? (
        <Routes>
          {authRoutes.map((route) => (
            <Route
              key={route.path}
              path={route.path}
              element={route.Component}
            />
          ))}
          <Route key={Date.now()} path="*" element={<Navigate to="/" />} />
        </Routes>
      ) : (
        <Routes>
          {publicRoutes.map((route, index) => (
            <Route
              key={route.path}
              path={route.path}
              element={route.Component}
            />
          ))}
          {authRoutes.map((route) => (
            <Route
              key={route.path}
              path={route.path}
              element={route.Component}
            />
          ))}
          <Route key={Date.now()} path="*" element={<Navigate to="/login" />} />
        </Routes>
      )}
    </div>
  );
};

export default AppRouter;
